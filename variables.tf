variable "name" {
  description = "The name of the firewall"
  type        = string
}

variable "droplet_ids" {
  description = "The IDs of the droplets for the firewall to apply to"
  type        = list(any)
  default     = []
}

variable "firewall_tags" {
  description = "The tags of vms to add to the firewall"
  type        = list(any)
  default     = []
}

variable "firewall_allowed_ips" {
  description = "A list of allowed ips"
  type        = list(any)
  default     = []
}
